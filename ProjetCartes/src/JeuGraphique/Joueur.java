package JeuGraphique;

import java.io.Serializable;
import java.util.ArrayList;

import InterfaceCarte.Carte;

public class Joueur implements Serializable{
	
	private String nom_joueur;
	private int age;
	private ArrayList<Carte> cartes_joueur = new ArrayList<Carte>();
	private boolean peux_jouer;
	
	public Joueur(String nom_joueur, int age) {
		this.nom_joueur = nom_joueur;
		this.age = age;
		this.peux_jouer=true;
	}
	
	public Joueur(String nom_joueur) {
		this.nom_joueur = nom_joueur;
		this.peux_jouer=true;
	}

	public ArrayList<Carte> getCartes_joueur() {
		return cartes_joueur;
	}

	public void setCartes_joueur(ArrayList<Carte> cartes_joueur) {
		this.cartes_joueur = cartes_joueur;
	}

	public String getNom_joueur() {
		return nom_joueur;
	}

	public int getAge() {
		return age;
	}

	public boolean isPeux_jouer() {
		return peux_jouer;
	}

	public void setPeux_jouer(boolean peux_jouer) {
		this.peux_jouer = peux_jouer;
	}

	public void ajout_deck(Carte carte) {
		cartes_joueur.add(carte);
	}
	
	public int taille_deck() {
		return cartes_joueur.size();
	}
	
	//recupère la carte que l'on souhaite poser
	public Carte recupere_carte_position(int n) {
		return cartes_joueur.get(n);
	}
	
	public void supprime_carte_position(Carte n) {
		this.cartes_joueur.remove(n);
	}

	@Override
	public String toString() {
		return "Joueur [nom_joueur=" + nom_joueur + ", age=" + age + ", cartes_joueur=" + cartes_joueur + "]";
	}
	
	
}