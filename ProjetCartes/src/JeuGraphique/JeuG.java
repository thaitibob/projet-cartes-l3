package JeuGraphique;

import Comparator.*;
import GestionFichiers.*;
import InterfaceCarte.Carte;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class JeuG implements Serializable{
	private int joueurs_en_cours;
	private int nombre_tours;
	
	private ArrayList<Carte> pioche;
	private ArrayList<Carte> defausse;
	private ArrayList<Carte> plateau;
	private ArrayList<Joueur> joueurs;
	
	private Comparator<Carte> c;
	
	private TauxReussiteCartes Statistiques;

	public JeuG(ArrayList<Joueur> joueurs, Comparator<Carte> c) throws IOException {
		Statistiques = new TauxReussiteCartes(c);
		this.pioche = Statistiques.getCartes();
		
		this.joueurs=joueurs;
		this.defausse = new ArrayList<Carte>();
		this.plateau=new ArrayList<Carte>();
		this.c = c;
	}
	
	public void ajout_joue(Carte i) throws IOException {
		Statistiques.ajout_carte_joue(i,c);
	}
	
	public void ajout_joue_reussi(Carte i) throws NumberFormatException, IOException {
		Statistiques.ajout_carte_reussi(i,c);
	}
	
	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	public void ajout_defausse(Carte carte) {
		defausse.add(carte);
	}

	public Carte pioche() {
		Carte distribution = pioche.get(0);
		pioche.remove(0);
		return distribution;
	}

	public Comparator<Carte> getC() {
		return c;
	}

	public int getNombre_tours() {
		return nombre_tours;
	}

	public void setNombre_tours(int nombre_tours) {
		this.nombre_tours = nombre_tours;
	}

	public int getJoueurs_en_cours() {
		return joueurs_en_cours;
	}

	public void setJoueurs_en_cours(int joueurs_en_cours) {
		this.joueurs_en_cours = joueurs_en_cours;
	}

	public Carte pioche(int n) {
		return pioche.get(n);
	}

	public ArrayList<Carte> getDefausse() {
		return defausse;
	}

	public int taille_pioche() {
		return pioche.size();
	}
	
	public void ajout_plateau(Carte carte) {
		this.plateau.add(carte);
		Collections.sort(this.plateau, c);
	}
	
	public void setC(Comparator<Carte> c) {
		this.c = c;
		Collections.sort(this.plateau, c);
	}

	public ArrayList<Carte> getPlateau() {
		return plateau;
	}

	public void Distribution() {
		if (joueurs.size() == 2 || joueurs.size() == 3) {
			for (Joueur j : joueurs) {
				for (int i = 0; i < 8; i++) {
					j.ajout_deck(pioche());
				}
			}
		}
		if (joueurs.size() == 4 || joueurs.size() == 5) {
			for (Joueur j : joueurs) {
				for (int i = 0; i < 5; i++) {
					j.ajout_deck(pioche());
				}
			}
		}
		if (joueurs.size() == 6 || joueurs.size() == 7 || joueurs.size() == 8) {
			for (Joueur j : joueurs) {
				for (int i = 0; i < 4; i++) {
					j.ajout_deck(pioche());
				}
			}
		}
	}

	public boolean verification_placement(Carte carte, int position) {
		int pos = Collections.binarySearch(this.plateau, carte, this.c);
		if (pos < 0) {
			pos = -pos - 1;
			if (pos == position) {
				return true;
			}
		} else {
			if (pos == position || pos+1==position) {
				return true;
			}
		}
		return false;
	}

	public void elimination() {
		for (Joueur j : joueurs) {
			if (j.taille_deck() != 0 && j.isPeux_jouer() == true) {
				j.setPeux_jouer(false);
			}
			if (j.taille_deck() == 0 && j.isPeux_jouer()) {
				j.ajout_deck(pioche());
			}
		}
	}
	
	public void verif_une_carte() {
		for(Joueur j : joueurs) {
			if(j.getCartes_joueur().size()>1) {
				j.setPeux_jouer(false);
			}
		}
	}

	public int prochain_joueur(int n) {

		if (n == joueurs.size() - 1) {
			n = 0;
			while (joueurs.get(n).isPeux_jouer() == false) {
				n++;
			}
		} else {
			n++;
			while (joueurs.get(n).isPeux_jouer() == false) {
				if (n == joueurs.size() - 1) {
					n = 0;
				} else {
					n++;
				}

			}
		}
		return n;
	}

	public boolean verif_tour() {
		for (Joueur j : joueurs) {
			if (j.getCartes_joueur().size() == 0) {
				return true;
			}
		}
		return false;
	}

	public boolean gagnant_final() {
		int compteur = 0;
		for (Joueur j : joueurs) {
			if (j.isPeux_jouer())
				compteur++;
		}
		return compteur == 1 ? true : false;
	}
}
