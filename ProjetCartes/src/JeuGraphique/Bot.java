package JeuGraphique;

import java.io.IOException;
import java.io.Serializable;
import java.util.Comparator;

import GestionFichiers.TauxReussiteCartes;
import InterfaceCarte.Carte;

public class Bot extends Joueur implements Serializable{
	
	Difficulte d;
	private TauxReussiteCartes statistiques;
	private Comparator<Carte> c;
	
	public Bot(String nom_bot, Difficulte t, Comparator<Carte> c) throws NumberFormatException, IOException {
		super(nom_bot);
		this.c=c;
		statistiques = new TauxReussiteCartes(c);
		this.d=t;
	}
	
	public Bot(String nom_bot, Comparator<Carte> c) throws NumberFormatException, IOException {
		super(nom_bot);
		statistiques = new TauxReussiteCartes(c);
	}
	
	

	public void setD(Difficulte d) {
		this.d = d;
	}
	
	public Difficulte getD() {
		return d;
	}

	public boolean joue_tour(Carte i) {
		if(d.equals(Difficulte.facile)) {
			int nombreAleatoire = 1 + (int)(Math.random() * ((100 - 1) + 1));
			if(nombreAleatoire<=25) {
				return true;
			}
		}
		if(d.equals(Difficulte.moyen)) {
			int nombreAleatoire = 1 + (int)(Math.random() * ((100 - 1) + 1));
			if(nombreAleatoire<=50) {
				return true;
			}
		}
		if(d.equals(Difficulte.difficile)) {
			int nombreAleatoire = 1 + (int)(Math.random() * ((100 - 1) + 1));
			if(nombreAleatoire<=75) {
				return true;
			}
		}
		if(d.equals(Difficulte.personnalise)) {
			double nombreAleatoire = 1 + (int)(Math.random() * ((1000000 - 1) + 1));
			double t = nombreAleatoire/10000;
			double taux = statistiques.get_taux(i);
			return t<taux ? true : false;
		}
		return false;
	}
	
	
}
