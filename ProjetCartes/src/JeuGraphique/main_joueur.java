package JeuGraphique;


import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import InterfaceCarte.Carte;

public class main_joueur implements MouseListener{

	private JPanel panel_main;
	
	private JLabel carte_select;
	
	private ArrayList<JLabel> jlabel;
	
	private int num_carte_select;

	/**
	 * Create the application.
	 */
	public main_joueur(JPanel panel_main) {
		this.panel_main=panel_main;
		jlabel = new ArrayList<JLabel>();
	}
	
	public void affichage_main(ArrayList<Carte> carte_joueur, String nom_joueur) {
		
		panel_main.removeAll();
		jlabel= new ArrayList<JLabel>();
		
		JLabel lblNewLabel = new JLabel("Joueur "+nom_joueur);
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(480, 11, 150, 29);
		panel_main.add(lblNewLabel);
		
		for (int i = 0; i < carte_joueur.size(); i++) {
			JLabel carte = new JLabel("");
			
			Image img = new ImageIcon(carte_joueur.get(i).getImage_lien()).getImage().getScaledInstance(120, 180,
					Image.SCALE_DEFAULT);
			carte.setIcon(new ImageIcon(img));
			carte.setBounds(calcul_position_cartes_x(carte_joueur.size(), i + 1), 65, 120, 180);
			carte.addMouseListener(this);
			jlabel.add(carte);
			panel_main.add(carte);
		}
		this.panel_main.repaint();
	}
	
	private int calcul_position_cartes_x(int nb_cartes_affiche, int numero_carte) {
		int x = (1030 - (11 * (nb_cartes_affiche - 1) + (nb_cartes_affiche * 120))) / 2;
		x = x + (numero_carte - 1) * 123 + 35;
		return x;
	}
	
	private void num_carte(int coordonnees) {
		for(int i=0 ; i< jlabel.size() ; i++) {
			if(calcul_position_cartes_x(jlabel.size(),i+1)==coordonnees) {
				this.setNum_carte_select(i);
			}
		}
	}
	
	public void setNum_carte_select(int num_carte_select) {
		this.num_carte_select = num_carte_select;
	}
	
	public boolean carte_selectionne() {
		return carte_select != null ? true : false;
	}
	
	public int getNum_carte_select() {
		return num_carte_select;
	}
	
	public void setCarte_select() {
		this.carte_select = null;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		for (JLabel j : jlabel) {
			if (e.getComponent().equals(j)) {
				if (carte_select != null && carte_select.equals(j)) {
					JLabel test = (JLabel) e.getComponent();
					test.setBorder(null);
					test.setBounds(e.getComponent().getX(), 65, 120, 180);
					j = test;
					this.carte_select = null;
				} else if (carte_select != null) {
					Border border = BorderFactory.createLineBorder(java.awt.Color.BLUE, 3);
					JLabel test = (JLabel) e.getComponent();
					test.setBorder(border);
					test.setBounds(e.getComponent().getX(), 55, 120, 180);
					num_carte(e.getComponent().getX());
					j = test;
					this.carte_select = test;
					for (JLabel i : jlabel) {
						if (!i.equals(carte_select)) {
							i.setBorder(null);
							i.setBounds(i.getX(), 65, 120, 180);
						}
					}
				} else {
					Border border = BorderFactory.createLineBorder(java.awt.Color.BLUE, 3);
					JLabel test = (JLabel) e.getComponent();
					test.setBorder(border);
					test.setBounds(e.getComponent().getX(), 55, 120, 180);
					num_carte(e.getComponent().getX());
					j = test;
					carte_select = test;
				}
			}
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (!e.getComponent().equals(this.carte_select)) {
			for (JLabel j : jlabel) {
				if (e.getComponent().equals(j)) {
					Border border = BorderFactory.createLineBorder(java.awt.Color.BLUE, 3);
					JLabel test = (JLabel) e.getComponent();
					test.setBorder(border);
					test.setBounds(e.getComponent().getX(), 55, 120, 180);
					j = test;
				}

			}
		}
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (!e.getComponent().equals(this.carte_select)) {
			for (JLabel j : jlabel) {
				if (e.getComponent().equals(j) && !j.equals(carte_select)) {
					JLabel test = (JLabel) e.getComponent();
					test.setBorder(null);
					test.setBounds(e.getComponent().getX(), 65, 120, 180);
					j = test;
				}
			}

		}		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
