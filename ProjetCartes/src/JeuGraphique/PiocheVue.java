package JeuGraphique;

import java.awt.Font;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import InterfaceCarte.Carte;

public class PiocheVue {
	
	private JPanel pioche;
	private ArrayList<JLabel> l;
	
	public PiocheVue(JPanel Pioche) {
		this.l = new ArrayList<JLabel>();
		this.pioche=Pioche;
		
		JLabel carte_pioche = new JLabel("");
		this.pioche.add(carte_pioche);
		JLabel defausse = new JLabel("");
		this.pioche.add(defausse);
		JLabel piochel = new JLabel("");
		this.pioche.add(piochel);
		
		l.add(carte_pioche);
		l.add(defausse);
		l.add(piochel);
		
	}
	
	public void affiche_pioche(Carte carte, int taille_defausse, int taille_pioche) {
		
		Image image = new ImageIcon(carte.getImage_lien()).getImage().getScaledInstance(100, 150, Image.SCALE_DEFAULT);
		l.get(0).setBounds(50,112, 100, 150);
		l.get(0).setIcon(new ImageIcon(image));
		
		l.get(1).setText("Cartes d\u00E9fausse : "+taille_defausse);
		l.get(1).setFont(new Font("Tahoma", Font.PLAIN, 15));
		l.get(1).setBounds(10, 11, 188, 31);
		
		l.get(2).setText("Pioche : "+taille_pioche);
		l.get(2).setFont(new Font("Tahoma", Font.PLAIN, 15));
		l.get(2).setBounds(65, 67, 75, 25);
		
		this.pioche.repaint();
	}
}
