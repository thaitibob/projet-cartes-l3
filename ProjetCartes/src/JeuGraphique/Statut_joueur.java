package JeuGraphique;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Statut_joueur {
	
	private JPanel liste_joueurs;
	private ArrayList<JLabel> statut;
	private ArrayList<Joueur> j;
	
	public Statut_joueur(JPanel liste_joueurs, ArrayList<Joueur> joueurs) {
		this.statut = new ArrayList<JLabel>();
		this.liste_joueurs=liste_joueurs;
		this.j=joueurs;
		
		JLabel joueur_1 = new JLabel("");
		joueur_1.setBounds(10, 11, 90, 50);
		liste_joueurs.add(joueur_1);
		
		JLabel joueur_2 = new JLabel("");
		joueur_2.setBounds(125, 11, 90, 50);
		liste_joueurs.add(joueur_2);

		JLabel joueur_3 = new JLabel("");
		joueur_3.setBounds(10, 72, 90, 50);
		liste_joueurs.add(joueur_3);
		
		JLabel joueur_4 = new JLabel("");
		joueur_4.setBounds(125, 72, 90, 50);
		liste_joueurs.add(joueur_4);
		
		JLabel joueur_5 = new JLabel("");
		joueur_5.setBounds(10, 133, 90, 50);
		liste_joueurs.add(joueur_5);
		
		JLabel joueur_6 = new JLabel("");
		joueur_6.setBounds(125, 133, 90, 50);
		liste_joueurs.add(joueur_6);
		
		JLabel joueur_7 = new JLabel("");
		joueur_7.setBounds(10, 194, 90, 50);
		liste_joueurs.add(joueur_7);
		
		JLabel joueur_8 = new JLabel("");
		joueur_8.setBounds(125, 194, 90, 50);
		liste_joueurs.add(joueur_8);
		
		this.statut.add(joueur_1);
		this.statut.add(joueur_2);
		this.statut.add(joueur_3);
		this.statut.add(joueur_4);
		this.statut.add(joueur_5);
		this.statut.add(joueur_6);
		this.statut.add(joueur_7);
		this.statut.add(joueur_8);
	}
	
	public void affichage_statut() {
		for(int i=0; i<j.size() ; i++) {
			if(j.get(i).isPeux_jouer()) {
				statut.get(i).setText("<html>"+j.get(i).getNom_joueur()+"<br>"+j.get(i).getCartes_joueur().size()+" cartes</html>");
				statut.get(i).setForeground(new Color(0,153,0));
			}else {
				statut.get(i).setText("<html>"+j.get(i).getNom_joueur()+"<br>�limin�</html>");
				statut.get(i).setForeground(Color.RED);
			}
		}
		this.liste_joueurs.repaint();
	}
}
