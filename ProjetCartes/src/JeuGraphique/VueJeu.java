package JeuGraphique;

import FinPartie.*;
import GestionFichiers.*;
import InterfaceCarte.Carte;
import InterfaceCarte.Invention;
import InterfaceCarte.Pays;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.Timer;
import javax.swing.border.Border;

import Comparator.ComparatorPIB;
import Comparator.ComparatorPollution;
import Comparator.ComparatorPopulation;
import Comparator.ComparatorSuperficie;
import Comparator.CompareDate;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;

public class VueJeu implements ActionListener {

	private JFrame frame;
	private JPanel panel_cartes_main;
	private JPanel panel_plateau;
	private JPanel timer;
	private JPanel liste_joueurs;
	private JPanel pioche_defausse;
	private JScrollPane scrollPane;

	private ArrayList<JButton> place_btn;
	private ArrayList<Joueur> joueurs;

	private int joueur_en_cours;
	private int nombre_tours;
	private static int temps;
	private Timer jolieTimer;

	private JeuG commande_jeu;
	private Statut_joueur sj;
	private PiocheVue pv;

	private main_joueur cartes_main;
	private JLabel lblNewLabel;

	private Comparator<Carte> c;

	public JLabel temps_tour;

	public VueJeu(ArrayList<Joueur> joueurs, Comparator<Carte> c) throws IOException {
		this.c = c;

		this.nombre_tours = -1;
		this.joueur_en_cours = 0;
		this.joueurs = joueurs;
		this.place_btn = new ArrayList<JButton>();

		initialize();

		this.cartes_main = new main_joueur(panel_cartes_main);
		this.panel_cartes_main.setLayout(null);

		try {
			this.commande_jeu = new JeuG(joueurs, this.c);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// on ajoute la premi�re carte sur le plateau
		this.commande_jeu.ajout_plateau(this.commande_jeu.pioche());
		this.commande_jeu.Distribution();
		this.sj = new Statut_joueur(this.liste_joueurs, this.joueurs);
		this.pv = new PiocheVue(this.pioche_defausse);
		// on affiche le jeu
		affichage_du_jeu();

	}

	public VueJeu(JeuG j) throws IOException {
		this.commande_jeu = j;
		this.joueurs = this.commande_jeu.getJoueurs();
		this.joueur_en_cours = this.commande_jeu.getJoueurs_en_cours();
		this.nombre_tours = this.commande_jeu.getNombre_tours();
		this.c=commande_jeu.getC();

		initialize();

		this.place_btn = new ArrayList<JButton>();
		this.cartes_main = new main_joueur(panel_cartes_main);
		this.panel_cartes_main.setLayout(null);

		this.sj = new Statut_joueur(this.liste_joueurs, this.joueurs);
		this.pv = new PiocheVue(this.pioche_defausse);
		// on affiche le jeu

		affichage_du_jeu();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frame = new JFrame();
		this.frame.setVisible(true);
		this.frame.setBounds(100, 100, 1301, 750);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.getContentPane().setLayout(null);

		menu_initialisation();

		this.panel_cartes_main = new JPanel();
		this.panel_cartes_main.setLayout(null);
		this.panel_cartes_main.setBounds(10, 425, 1030, 255);
		this.frame.getContentPane().add(this.panel_cartes_main);

		this.scrollPane = new JScrollPane();
		this.scrollPane.setBounds(10, 11, 1030, 399);
		this.frame.getContentPane().add(this.scrollPane);

		this.panel_plateau = new JPanel();
		this.scrollPane.setViewportView(this.panel_plateau);
		this.panel_plateau.setLayout(null);

		this.liste_joueurs = new JPanel();
		this.liste_joueurs.setBounds(1050, 108, 225, 266);
		this.frame.getContentPane().add(this.liste_joueurs);

		this.pioche_defausse = new JPanel();
		this.pioche_defausse.setBounds(1050, 384, 225, 296);
		this.frame.getContentPane().add(this.pioche_defausse);

		this.timer = new JPanel();
		this.timer.setBounds(1050, 11, 225, 86);
		this.frame.getContentPane().add(this.timer);

		this.panel_cartes_main.setBorder(BorderFactory.createLineBorder(java.awt.Color.BLUE, 3));
		this.panel_plateau.setBorder(BorderFactory.createLineBorder(java.awt.Color.ORANGE, 3));
		this.liste_joueurs.setBorder(BorderFactory.createLineBorder(java.awt.Color.BLACK, 2));
		liste_joueurs.setLayout(null);

		this.pioche_defausse.setBorder(BorderFactory.createLineBorder(java.awt.Color.BLACK, 2));
		pioche_defausse.setLayout(null);

		this.timer.setBorder(BorderFactory.createLineBorder(java.awt.Color.BLACK, 2));
		timer.setLayout(null);

		temps_tour = new JLabel();
		temps_tour.setFont(new Font("Lucida Grande", Font.BOLD, 32));
		temps_tour.setBounds(71, 6, 136, 74);
		timer.add(temps_tour);

	}

	public void setC(Comparator<Carte> c) {
		this.c = c;
		commande_jeu.setC(c);
	}

	private void temps_restant(int n) {
		this.temps_tour.setText(String.valueOf(n) + " sec");
	}

	private void affichage_du_jeu() throws IOException {
		this.nombre_tours++;
		VueJeu.temps = 31;

		int delay = 1000; // milliseconds
		jolieTimer = new Timer(delay, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				temps_restant(temps);
				if (temps == 0) {
					((Timer) e.getSource()).stop();
					joueur_en_cours = commande_jeu.prochain_joueur(joueur_en_cours);
					try {
						affichage_du_jeu();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				temps--;
			}
		});
		jolieTimer.start();
		if (joueurs.get(this.joueur_en_cours) instanceof Bot) {
			try {
				Carte i = joueurs.get(this.joueur_en_cours).recupere_carte_position(0);
				if (((Bot) joueurs.get(this.joueur_en_cours)).joue_tour(i)) {
					commande_jeu.ajout_joue_reussi(i);
					commande_jeu.ajout_joue(i);
					this.commande_jeu.ajout_plateau(i);
					joueurs.get(this.joueur_en_cours).supprime_carte_position(i);
					if (joueurs.get(this.joueur_en_cours).getCartes_joueur().size() == 0) {
						this.commande_jeu.verif_une_carte();
					}
					if (this.commande_jeu.prochain_joueur(this.joueur_en_cours) < this.joueur_en_cours) {
						if (this.commande_jeu.verif_tour()) {
							this.commande_jeu.elimination();
						}
					}
				} else {
					commande_jeu.ajout_joue(i);
					commande_jeu.ajout_defausse(i);
					joueurs.get(this.joueur_en_cours).supprime_carte_position(i);
					joueurs.get(this.joueur_en_cours).ajout_deck(commande_jeu.pioche());
					if (this.commande_jeu.prochain_joueur(this.joueur_en_cours) < this.joueur_en_cours) {
						if (this.commande_jeu.verif_tour()) {
							this.commande_jeu.elimination();
						}
					}
				}
				if (this.commande_jeu.gagnant_final()) {
					jolieTimer.stop();
					for (Joueur j : joueurs) {
						if (j.isPeux_jouer()) {
							Vainqueur v = new Vainqueur(j.getNom_joueur(), joueurs, c);
						}
					}
					frame.dispose();
					frame = null;
					System.gc();
				} else {
					jolieTimer.stop();
					this.joueur_en_cours = this.commande_jeu.prochain_joueur(this.joueur_en_cours);
					affichage_du_jeu();
				}

			} catch (Exception e) {
				jolieTimer.stop();
				Vainqueur v = new Vainqueur("Egalité", joueurs, c);
				// System.exit(0);
				frame.dispose();
				frame = null;
				System.gc();
			}
		} else {
			this.panel_plateau.removeAll();
			this.place_btn = new ArrayList<JButton>();

			int position = 0;
			int taille_plateau = this.commande_jeu.getPlateau().size() * 100
					+ (this.commande_jeu.getPlateau().size() + 1) * 43
					+ (10 + 5 * 2 * this.commande_jeu.getPlateau().size());
			if (taille_plateau > 1030) {
				this.panel_plateau.setPreferredSize(new Dimension(taille_plateau, this.panel_cartes_main.getHeight()));
			}
			for (Carte c : this.commande_jeu.getPlateau()) {
				JButton btnNewButton_1 = new JButton("+");
				btnNewButton_1.setBounds(5 + 153 * position, 275, 43, 30);
				panel_plateau.add(btnNewButton_1);

				JLabel carte = new JLabel("");
				if (this.c instanceof CompareDate) {
					carte = new JLabel("<html><center>" + ((Invention) c).getDate_invention() + "<center><center>"
							+ c.getNom_Carte() + "<center></html>");
				}
				if (this.c instanceof ComparatorPIB) {
					carte = new JLabel("<html><center>" + ((Pays) c).getPib() + "<center><center>" + c.getNom_Carte()
							+ "<center></html>");
				}
				if (this.c instanceof ComparatorPollution) {
					carte = new JLabel("<html><center>" + ((Pays) c).getPollution() + "<center><center>"
							+ c.getNom_Carte() + "<center></html>");
				}
				if (this.c instanceof ComparatorPopulation) {
					carte = new JLabel("<html><center>" + ((Pays) c).getPopulation() + "<center><center>"
							+ c.getNom_Carte() + "<center></html>");
				}
				if (this.c instanceof ComparatorSuperficie) {
					carte = new JLabel("<html><center>" + ((Pays) c).getSuperficie() + "<center><center>"
							+ c.getNom_Carte() + "<center></html>");
				}

				carte.setFont(new Font("Tempus Sans ITC", Font.BOLD, 11));
				carte.setForeground(Color.RED);
				carte.setBounds(153 * position + 53, 250, 100, 80);
				Border border = BorderFactory.createLineBorder(java.awt.Color.BLACK, 1);
				carte.setBorder(border);
				Image image = new ImageIcon(c.getImage_lien_solution()).getImage().getScaledInstance(125, 180,
						Image.SCALE_DEFAULT);
				JLabel affichage_carte = new JLabel("");
				affichage_carte.setIcon(new ImageIcon(image));
				affichage_carte.setBounds(153 * position + 37, 60, 125, 180);
				panel_plateau.add(carte);
				panel_plateau.add(affichage_carte);

				position++;

				btnNewButton_1.addActionListener(this);
				place_btn.add(btnNewButton_1);
			}
			JButton btnNewButton_2 = new JButton("+");
			btnNewButton_2.setBounds(5 + 153 * position, 275, 43, 30);
			btnNewButton_2.addActionListener(this);
			panel_plateau.add(btnNewButton_2);
			place_btn.add(btnNewButton_2);
			
			lblNewLabel = new JLabel("");
			
			if (this.c instanceof CompareDate) {
				lblNewLabel = new JLabel("Timeline : Comparaison par date | Nombre de tours : " + this.nombre_tours);
			}
			if (this.c instanceof ComparatorPIB) {
				lblNewLabel = new JLabel("Cardline : Comparaison par PIB | Nombre de tours : " + this.nombre_tours);
			}
			if (this.c instanceof ComparatorPollution) {
				lblNewLabel = new JLabel("Cardline : Comparaison par Pollution | Nombre de tours : " + this.nombre_tours);
			}
			if (this.c instanceof ComparatorPopulation) {
				lblNewLabel = new JLabel("Cardline : Comparaison par Population | Nombre de tours : " + this.nombre_tours);
			}
			if (this.c instanceof ComparatorSuperficie) {
				lblNewLabel = new JLabel("Cardline : Comparaison par Superficie | Nombre de tours : " + this.nombre_tours);
			}
			
			lblNewLabel.setFont(new Font("Tahoma", Font.ITALIC, 16));
			lblNewLabel.setBackground(Color.GRAY);
			lblNewLabel.setBounds(23, 11, 600, 30);
			this.lblNewLabel.setOpaque(false);
			panel_plateau.add(lblNewLabel);

			this.cartes_main.affichage_main(joueurs.get(this.joueur_en_cours).getCartes_joueur(),
					this.joueurs.get(this.joueur_en_cours).getNom_joueur());
			this.sj.affichage_statut();
			this.pv.affiche_pioche(this.commande_jeu.pioche(0), this.commande_jeu.getDefausse().size(),
					this.commande_jeu.taille_pioche());

			this.panel_plateau.repaint();
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int pos = -1, tmp = 0;
		for (JButton b : place_btn) {
			if (e.getSource().equals(b)) {
				pos = tmp;
			}
			tmp++;
		}
		try {
			if (this.cartes_main.carte_selectionne()) {
				jolieTimer.stop();
				Carte i = joueurs.get(this.joueur_en_cours)
						.recupere_carte_position(this.cartes_main.getNum_carte_select());
				if (commande_jeu.verification_placement(i, pos)) {
					try {
						commande_jeu.ajout_joue_reussi(i);
						commande_jeu.ajout_joue(i);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					this.cartes_main.setCarte_select();
					this.commande_jeu.ajout_plateau(i);
					joueurs.get(this.joueur_en_cours).supprime_carte_position(i);
					if (joueurs.get(this.joueur_en_cours).getCartes_joueur().size() == 0) {
						this.commande_jeu.verif_une_carte();
					}
					if (this.commande_jeu.prochain_joueur(this.joueur_en_cours) < this.joueur_en_cours) {
						if (this.commande_jeu.verif_tour()) {
							this.commande_jeu.elimination();
						}
					}
				} else {
					try {
						commande_jeu.ajout_joue(i);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					this.cartes_main.setCarte_select();
					commande_jeu.ajout_defausse(i);
					joueurs.get(this.joueur_en_cours).supprime_carte_position(i);
					joueurs.get(this.joueur_en_cours).ajout_deck(commande_jeu.pioche());
					if (this.commande_jeu.prochain_joueur(this.joueur_en_cours) < this.joueur_en_cours) {
						if (this.commande_jeu.verif_tour()) {
							this.commande_jeu.elimination();
						}
					}
				}
				if (this.commande_jeu.gagnant_final()) {
					jolieTimer.stop();
					for (Joueur j : joueurs) {
						if (j.isPeux_jouer()) {
							try {
								Vainqueur v = new Vainqueur(j.getNom_joueur(), joueurs, c);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
					frame.dispose();
					frame = null;
					System.gc();
					System.exit(0);
				} else {
					this.joueur_en_cours = this.commande_jeu.prochain_joueur(this.joueur_en_cours);
					try {
						affichage_du_jeu();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		} catch (Exception e2) {
			jolieTimer.stop();
			try {
				Vainqueur v = new Vainqueur("Egalité", joueurs, c);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			// System.exit(0);
			frame.dispose();
			frame = null;
			System.gc();
		}

	}

	private void menu_initialisation() {
		boolean verif_bot = false;
		Difficulte d = null;

		for (int i = 0; i < joueurs.size(); i++) {
			if (joueurs.get(i) instanceof Bot) {
				d = ((Bot) joueurs.get(i)).getD();
				verif_bot = true;
			}
		}

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnMenu = new JMenu("Menu");
		ActionListener ChoixMenu = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (e.getActionCommand().equals("Nouvelle Partie")) {
						ArrayList<Joueur> nouvelle_partie = new ArrayList<Joueur>();
						for (Joueur j : joueurs) {
							if (j instanceof Bot) {
								nouvelle_partie.add(new Bot(j.getNom_joueur(), ((Bot) j).getD(), c));
							} else {
								nouvelle_partie.add(new Joueur(j.getNom_joueur(), j.getAge()));
							}
						}
						VueJeu v = new VueJeu(nouvelle_partie, c);
						frame.dispose();
						frame = null;
						System.gc();
					}
					if (e.getActionCommand().equals("Charger Partie")) {
						jolieTimer.stop();
						SauvegardePartie s = new SauvegardePartie();
						JeuG v = s.chargement();
						if (v == null) {
							JOptionPane.showMessageDialog(frame, "Erreur de chargement (verifier bien le fichier)");
							jolieTimer.start();
						} else {
							VueJeu t = new VueJeu(v);
							frame.dispose();
							frame = null;
							System.gc();
						}
					}
					if (e.getActionCommand().equals("Sauvegarder Partie")) {
						jolieTimer.stop();
						commande_jeu.setJoueurs_en_cours(joueur_en_cours);
						commande_jeu.setNombre_tours(nombre_tours);
						SauvegardePartie s = new SauvegardePartie();
						if (!s.sauvegarde(commande_jeu)) {
							JOptionPane.showMessageDialog(frame, "Erreur de sauvegarde (verifier extension .ser)");
							jolieTimer.start();
						} else {
							JOptionPane.showMessageDialog(frame, "Partie Sauvegardée");
							jolieTimer.start();
						}
					}
					if (e.getActionCommand().equals("Quitter")) {
						frame.dispose();
						System.gc();
						System.exit(0);
					}

					if (e.getActionCommand().equals("Facile")) {
						for (Joueur j : joueurs) {
							if (j instanceof Bot) {
								((Bot) j).setD(Difficulte.facile);
							}
						}
					}
					if (e.getActionCommand().equals("Moyen")) {
						for (Joueur j : joueurs) {
							if (j instanceof Bot) {
								((Bot) j).setD(Difficulte.moyen);
							}
						}
					}
					if (e.getActionCommand().equals("Difficile")) {
						for (Joueur j : joueurs) {
							if (j instanceof Bot) {
								((Bot) j).setD(Difficulte.difficile);
							}
						}
					}
					if (e.getActionCommand().equals("Personnalisé")) {
						for (Joueur j : joueurs) {
							if (j instanceof Bot) {
								((Bot) j).setD(Difficulte.personnalise);
							}
						}
					}	
					if (e.getActionCommand().equals("PIB")) {
						jolieTimer.stop();
						setC(new ComparatorPIB());
						affichage_du_jeu();
					}
					if (e.getActionCommand().equals("Pollution")) {
						jolieTimer.stop();
						setC(new ComparatorPollution());
						affichage_du_jeu();
					}
					if (e.getActionCommand().equals("Population")) {
						jolieTimer.stop();
						setC(new ComparatorPopulation());
						affichage_du_jeu();
					}
					if (e.getActionCommand().equals("Superficie")) {
						jolieTimer.stop();
						setC(new ComparatorSuperficie());
						affichage_du_jeu();
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};

		menuBar.add(mnMenu);

		JMenuItem mntmNouvellePartie = new JMenuItem("Nouvelle Partie");
		mnMenu.add(mntmNouvellePartie);
		mntmNouvellePartie.addActionListener(ChoixMenu);

		JSeparator separator = new JSeparator();
		mnMenu.add(separator);

		JMenuItem mntmChargerPartie = new JMenuItem("Charger Partie");
		mnMenu.add(mntmChargerPartie);
		mntmChargerPartie.addActionListener(ChoixMenu);

		JMenuItem mntmSauvegarderPartie = new JMenuItem("Sauvegarder Partie");
		mnMenu.add(mntmSauvegarderPartie);
		mntmSauvegarderPartie.addActionListener(ChoixMenu);

		JSeparator separator_1 = new JSeparator();
		mnMenu.add(separator_1);

		JMenuItem mntmQuitter = new JMenuItem("Quitter");
		mnMenu.add(mntmQuitter);
		mntmQuitter.addActionListener(ChoixMenu);
		
		if(!(c instanceof CompareDate)) {
			JMenu mnComparaison = new JMenu("Critère");
			menuBar.add(mnComparaison);
			
			JRadioButtonMenuItem rdbtnmntmPIB = new JRadioButtonMenuItem("PIB");
			mnComparaison.add(rdbtnmntmPIB);
			rdbtnmntmPIB.addActionListener(ChoixMenu);
			if (c instanceof ComparatorPIB) {
				rdbtnmntmPIB.setSelected(true);
			}

			JRadioButtonMenuItem rdbtnmntmPollution = new JRadioButtonMenuItem("Pollution");
			mnComparaison.add(rdbtnmntmPollution);
			rdbtnmntmPollution.addActionListener(ChoixMenu);
			if (c instanceof ComparatorPollution) {
				rdbtnmntmPollution.setSelected(true);
			}

			JRadioButtonMenuItem rdbtnmntmPopulation = new JRadioButtonMenuItem("Population");
			mnComparaison.add(rdbtnmntmPopulation);
			rdbtnmntmPopulation.addActionListener(ChoixMenu);
			if (c instanceof ComparatorPopulation) {
				rdbtnmntmPopulation.setSelected(true);
			}

			JRadioButtonMenuItem rdbtnmntmSuperficie = new JRadioButtonMenuItem("Superficie");
			mnComparaison.add(rdbtnmntmSuperficie);
			rdbtnmntmSuperficie.addActionListener(ChoixMenu);
			if (c instanceof ComparatorSuperficie) {
				rdbtnmntmSuperficie.setSelected(true);
			}

			ButtonGroup group1 = new ButtonGroup();
			group1.add(rdbtnmntmPIB);
			group1.add(rdbtnmntmPollution);
			group1.add(rdbtnmntmPopulation);
			group1.add(rdbtnmntmSuperficie);
			
		}

		if (verif_bot) {
			JMenu mnDifficult = new JMenu("Difficulté");
			menuBar.add(mnDifficult);

			JRadioButtonMenuItem rdbtnmntmFacile = new JRadioButtonMenuItem("Facile");
			mnDifficult.add(rdbtnmntmFacile);
			rdbtnmntmFacile.addActionListener(ChoixMenu);
			if (d.equals(Difficulte.facile)) {
				rdbtnmntmFacile.setSelected(true);
			}

			JRadioButtonMenuItem rdbtnmntmMoyen = new JRadioButtonMenuItem("Moyen");
			mnDifficult.add(rdbtnmntmMoyen);
			rdbtnmntmMoyen.addActionListener(ChoixMenu);
			if (d.equals(Difficulte.moyen)) {
				rdbtnmntmFacile.setSelected(true);
			}

			JRadioButtonMenuItem rdbtnmntmDifficile = new JRadioButtonMenuItem("Difficile");
			mnDifficult.add(rdbtnmntmDifficile);
			rdbtnmntmDifficile.addActionListener(ChoixMenu);
			if (d.equals(Difficulte.difficile)) {
				rdbtnmntmFacile.setSelected(true);
			}

			JRadioButtonMenuItem rdbtnmntmPersonnalis = new JRadioButtonMenuItem("Personnalisé");
			mnDifficult.add(rdbtnmntmPersonnalis);
			rdbtnmntmPersonnalis.addActionListener(ChoixMenu);
			if (d.equals(Difficulte.personnalise)) {
				rdbtnmntmFacile.setSelected(true);
			}

			ButtonGroup group2 = new ButtonGroup();
			group2.add(rdbtnmntmFacile);
			group2.add(rdbtnmntmMoyen);
			group2.add(rdbtnmntmDifficile);
			group2.add(rdbtnmntmPersonnalis);
		}

	}

}
