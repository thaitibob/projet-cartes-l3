package InterfaceCarte;

import java.io.Serializable;

public class Pays implements Serializable,Carte{
	private String nom_pays;
	private int superficie;
	private int population;
	private int pib;
	private float pollution;
	private String nom_image;
	private String nom_image_solution;
	
	
	public Pays(String nom_pays, int superficie, int population, int pib, float pollution, String nom_image) {
		super();
		this.nom_pays=nom_pays;
		this.superficie=superficie;
		this.population=population;
		this.pib=pib;
		this.pollution=pollution;
		this.nom_image=nom_image+".jpeg";
		this.nom_image_solution=nom_image+"_reponse.jpeg";
		
	}

	@Override
	public String getNom_Carte() {
		return nom_pays;
	}
	
	@Override
	public String getImage_lien() {
		String chemin = System.getProperty("user.dir");
		return chemin+"/data/cardline/cards/"+nom_image;
	}

	@Override
	public String getImage_lien_solution() {
		String chemin = System.getProperty("user.dir");
		return chemin+"/data/cardline/cards/"+nom_image_solution;
	}

	public int getSuperficie() {
		return superficie;
	}

	public int getPopulation() {
		return population;
	}

	public int getPib() {
		return pib;
	}

	public float getPollution() {
		return pollution;
	}

	@Override
	public String toString() {
		return "Pays [nom_pays=" + nom_pays + ", superficie=" + superficie + ", population=" + population + ", pib="
				+ pib + ", pollution=" + pollution + ", nom_image=" + nom_image + ", nom_image_solution="
				+ nom_image_solution + "]";
	}

}
