package InterfaceCarte;

import java.io.Serializable;

public class Invention implements Serializable,Carte{
	private String nom_invention;
	private int date_invention;
	private String image_lien;
	private String image_lien_date;
	
	public Invention(String nom_invention, int date_invention, String image_lien, String image_lien_date) {
		super();
		this.nom_invention = nom_invention;
		this.date_invention = date_invention;
		this.image_lien = image_lien;
		this.image_lien_date = image_lien_date;
	}

	@Override
	public String getNom_Carte() {
		return nom_invention;
	}
	
	@Override
	public String getImage_lien() {
		String chemin = System.getProperty("user.dir");
		return chemin+"/data/timeline/cards/"+image_lien;
	}

	@Override
	public String getImage_lien_solution() {
		String chemin = System.getProperty("user.dir");
		return chemin+"/data/timeline/cards/"+image_lien_date;
	}

	public int getDate_invention() {
		return date_invention;
	}

	@Override
	public String toString() {
		return "Invention [nom_invention=" + nom_invention + ", date_invention=" + date_invention + ", image_lien="
				+ image_lien + ", image_lien_date=" + image_lien_date + "]";
	}
		
}
