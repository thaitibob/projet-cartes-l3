package FinPartie;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import Comparator.CompareDate;
import InterfaceCarte.Carte;
import JeuGraphique.Bot;
import JeuGraphique.Joueur;
import JeuGraphique.VueJeu;
import LancementPartie.MenuPrincipal;

import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.awt.event.ActionEvent;

public class Vainqueur {
	
	private String nom;
	private JFrame frame;
	private ArrayList<Joueur> joueur = new ArrayList<Joueur>();
	private Comparator<Carte> comparator;

	/**
	 * Create the application.
	 * @throws IOException 
	 */
	public Vainqueur(String nom,ArrayList<Joueur> j,Comparator<Carte> comparator) throws IOException {
		joueur = j;
		this.nom=nom;
		this.comparator=comparator;
		ajout_vainqueur_fichier();
		initialize();
	}
	
	private void ajout_vainqueur_fichier() throws IOException {
		if(!nom.equals("Egalité")) {
			ArrayList<String> t = new ArrayList<String>();
			String chemin = System.getProperty("user.dir");
			BufferedReader br = null;
			String line = "";
			try {
				br = new BufferedReader(new FileReader(chemin + "/data/TopJoueurs.csv"));
				int compteur=0;
				while ((line = br.readLine()) != null) {
					String[] chaine = line.split(";");
					if(chaine[0].equals(nom)) {
						t.add(chaine[0]+";"+String.valueOf(Integer.parseInt(chaine[1])+1)+"\n");
						compteur++;
					}else {
						t.add(chaine[0]+";"+chaine[1]+"\n");
					}
				}
				if(compteur==0) {
					t.add(nom+";"+String.valueOf(1));
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			br.close();
			FileWriter fw = new FileWriter(chemin + "/data/TopJoueurs.csv",false);
			for(String ligne : t){
				fw.write(ligne);
			}
			fw.close();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setBackground(Color.orange);
		
		JLabel VainqueurLabel = new JLabel(nom);
		VainqueurLabel.setForeground(Color.RED);
		VainqueurLabel.setFont(new Font("Lucida Grande", Font.BOLD, 27));
		VainqueurLabel.setBounds(110, 54, 245, 51);
		VainqueurLabel.setHorizontalAlignment(JLabel.CENTER);
		VainqueurLabel.setVerticalAlignment(JLabel.CENTER);
		frame.getContentPane().add(VainqueurLabel);
		
		JButton NouvellePartieBoutton = new JButton("Nouvelle Partie");
		NouvellePartieBoutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Joueur> nouvelle_partie = new ArrayList<Joueur>();
				for (Joueur j : joueur) {
					if (j instanceof Bot) {
						try {
							nouvelle_partie.add(new Bot(j.getNom_joueur(), ((Bot) j).getD(),comparator));
						}catch (IOException e1) {
							e1.printStackTrace();
						}
					} else {
						nouvelle_partie.add(new Joueur(j.getNom_joueur(), j.getAge()));
					}
				}
				try {
					VueJeu v = new VueJeu(nouvelle_partie, new CompareDate());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				frame.dispose();
				frame = null;
				System.gc();
			}
		});
		NouvellePartieBoutton.setBounds(160, 142, 151, 39);
		frame.getContentPane().add(NouvellePartieBoutton);
		
		JButton buttonQuitter = new JButton("Quitter");
		buttonQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				System.exit(0);
			}
		});
		buttonQuitter.setBounds(160, 243, 151, 39);
		frame.getContentPane().add(buttonQuitter);
		
		JButton buttonStatJoueurs = new JButton("Menu");
		buttonStatJoueurs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPrincipal m = new MenuPrincipal();
				frame.dispose();
				frame=null;
				System.gc();
			}
		});
		buttonStatJoueurs.setBounds(160, 192, 151, 39);
		frame.getContentPane().add(buttonStatJoueurs);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}

}
