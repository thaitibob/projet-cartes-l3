package GestionFichiers;

import JeuGraphique.*;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class SauvegardePartie implements Serializable {

	public SauvegardePartie() {

	}

	public boolean sauvegarde(JeuG j) throws FileNotFoundException, IOException {
		// mettre extension .ser
		JFileChooser chooser = new JFileChooser();
		chooser.showSaveDialog(null);
		File f = chooser.getSelectedFile();
		String[] tab = String.valueOf(f).split("/");
		String fichier = tab[tab.length-1];
		String[] tab1=fichier.split("\\.");
		if(tab1.length==2 && tab1[1].equals("ser")) {
			ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream(f));
			obj.writeObject(j);
			obj.close();
			return true;
		}
		return false;

	}

	public JeuG chargement() throws FileNotFoundException, IOException, ClassNotFoundException {
		JeuG v = null;
		JFileChooser fileopen = new JFileChooser();

		int ret = fileopen.showDialog(null, "Ouvrir");

		if (ret == JFileChooser.APPROVE_OPTION) {
			File file = fileopen.getSelectedFile();
			System.out.println(file);
			try {
				ObjectInputStream obj = new ObjectInputStream(new FileInputStream(file));
				v = (JeuG) obj.readObject();
				obj.close();
			} catch (Exception e) {
				v=null;
			}

		}

		return v;

	}

}
