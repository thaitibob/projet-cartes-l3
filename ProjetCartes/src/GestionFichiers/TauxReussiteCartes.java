package GestionFichiers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JLabel;

import Comparator.ComparatorPIB;
import Comparator.ComparatorPollution;
import Comparator.ComparatorPopulation;
import Comparator.ComparatorSuperficie;
import Comparator.CompareDate;
import InterfaceCarte.Carte;
import InterfaceCarte.Invention;
import InterfaceCarte.Pays;
import JeuGraphique.*;

public class TauxReussiteCartes implements Serializable{

	private HashMap<String, Integer> taux_joue;
	private HashMap<String, Integer> taux_reussi;

	ArrayList<Carte> cartes = new ArrayList<Carte>();

	public TauxReussiteCartes(Comparator<Carte> comparator) throws NumberFormatException, IOException {

		taux_joue = new HashMap<String, Integer>();
		taux_reussi = new HashMap<String, Integer>();
		
		BufferedReader fichier_source = null;

		try {
			String chemin = System.getProperty("user.dir");
			if(comparator instanceof CompareDate) {
				fichier_source = new BufferedReader(new FileReader(chemin + "/data/timeline/timeline.csv"));
			}else {
				fichier_source = new BufferedReader(new FileReader(chemin + "/data/cardline/cardline.csv"));
			}
			String chaine;
			int i = 1;

			while ((chaine = fichier_source.readLine()) != null) {
				if (i > 1) {
					String[] tabChaine = chaine.split(";");
					if(comparator instanceof CompareDate) {
						Carte carte = new Invention(tabChaine[0], Integer.parseInt(tabChaine[1]),
								tabChaine[2] + ".jpeg", tabChaine[2] + "_date.jpeg");
						cartes.add(carte);
						taux_joue.put(tabChaine[0], Integer.parseInt(tabChaine[3]));
						taux_reussi.put(tabChaine[0], Integer.parseInt(tabChaine[4]));
					}else {
						tabChaine[4]=tabChaine[4].replace(",", ".");
						Carte carte = new Pays(tabChaine[0], Integer.parseInt(tabChaine[1]),Integer.parseInt(tabChaine[2]),Integer.parseInt(tabChaine[3]),Float.parseFloat(tabChaine[4]),tabChaine[5]);
						cartes.add(carte);
						if(comparator instanceof ComparatorPIB) {
							taux_joue.put(tabChaine[0], Integer.parseInt(tabChaine[8]));
							taux_reussi.put(tabChaine[0], Integer.parseInt(tabChaine[12]));
						}
						if(comparator instanceof ComparatorPollution) {
							taux_joue.put(tabChaine[0], Integer.parseInt(tabChaine[9]));
							taux_reussi.put(tabChaine[0], Integer.parseInt(tabChaine[13]));
						}
						if(comparator instanceof ComparatorPopulation) {
							taux_joue.put(tabChaine[0], Integer.parseInt(tabChaine[7]));
							taux_reussi.put(tabChaine[0], Integer.parseInt(tabChaine[11]));
						}
						if(comparator instanceof ComparatorSuperficie) {
							taux_joue.put(tabChaine[0], Integer.parseInt(tabChaine[6]));
							taux_reussi.put(tabChaine[0], Integer.parseInt(tabChaine[10]));
						}
					}

				}
				i++;
			}
			fichier_source.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier est introuvable !");
			System.out.println(e);
		}

		Collections.shuffle(cartes);
	}

	public ArrayList<Carte> getCartes() {
		return cartes;
	}

	public void ajout_carte_joue(Carte carte, Comparator<Carte> comparator) throws IOException {
		ArrayList<String> t = new ArrayList<String>();
		String chemin = System.getProperty("user.dir");
		BufferedReader br = null;
		String line = "";
		try {
			if(comparator instanceof CompareDate) {
				br = new BufferedReader(new FileReader(chemin + "/data/timeline/timeline.csv"));
			}else {
				br = new BufferedReader(new FileReader(chemin + "/data/cardline/cardline.csv"));
			}
			while ((line = br.readLine()) != null) {
				String[] chaine = line.split(";");
				if(comparator instanceof CompareDate) {
					if(chaine[0].equals(carte.getNom_Carte())) {
						chaine[3]=String.valueOf(Integer.parseInt(chaine[3])+1);
						t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+"\n");
					}else {
						t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+"\n");
					}
				}else {		
					if(comparator instanceof ComparatorPIB) {
						if(chaine[0].equals(carte.getNom_Carte())) {
							chaine[8]=String.valueOf(Integer.parseInt(chaine[8]+1));
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}else {
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}
					}
					if(comparator instanceof ComparatorPollution) {
						if(chaine[0].equals(carte.getNom_Carte())) {
							chaine[9]=String.valueOf(Integer.parseInt(chaine[9]+1));
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}else {
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}
					}
					if(comparator instanceof ComparatorPopulation) {
						if(chaine[0].equals(carte.getNom_Carte())) {
							chaine[7]=String.valueOf(Integer.parseInt(chaine[7]+1));
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}else {
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}
					}
					if(comparator instanceof ComparatorSuperficie) {
						if(chaine[0].equals(carte.getNom_Carte())) {
							chaine[6]=String.valueOf(Integer.parseInt(chaine[6]+1));
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}else {
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}
					}
				}	
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		br.close();
		FileWriter fw = null;
		if(comparator instanceof CompareDate) {
			fw = new FileWriter(chemin + "/data/timeline/timeline.csv",false);
		}else {
			fw = new FileWriter(chemin + "/data/cardline/cardline.csv",false);
		}
		for(String ligne : t){
			fw.write(ligne);
		}
		fw.close();
	}

	public void ajout_carte_reussi(Carte carte, Comparator<Carte> comparator) throws NumberFormatException, IOException {
		ArrayList<String> t = new ArrayList<String>();
		String chemin = System.getProperty("user.dir");
		BufferedReader br = null;
		String line = "";
		try {
			if(comparator instanceof CompareDate) {
				br = new BufferedReader(new FileReader(chemin + "/data/timeline/timeline.csv"));
			}else {
				br = new BufferedReader(new FileReader(chemin + "/data/cardline/cardline.csv"));
			}
			while ((line = br.readLine()) != null) {
				String[] chaine = line.split(";");
				
				if(comparator instanceof CompareDate) {
					if(chaine[0].equals(carte.getNom_Carte())) {
						chaine[4]=String.valueOf(Integer.parseInt(chaine[4])+1);
						t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+"\n");
					}else {
						t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+"\n");
					}
				}else {		
					if(comparator instanceof ComparatorPIB) {
						if(chaine[0].equals(carte.getNom_Carte())) {
							chaine[12]=String.valueOf(Integer.parseInt(chaine[12]+1));
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}else {
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}
					}
					if(comparator instanceof ComparatorPollution) {
						if(chaine[0].equals(carte.getNom_Carte())) {
							chaine[13]=String.valueOf(Integer.parseInt(chaine[13]+1));
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}else {
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}
					}
					if(comparator instanceof ComparatorPopulation) {
						if(chaine[0].equals(carte.getNom_Carte())) {
							chaine[11]=String.valueOf(Integer.parseInt(chaine[11]+1));
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}else {
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}
					}
					if(comparator instanceof ComparatorSuperficie) {
						if(chaine[0].equals(carte.getNom_Carte())) {
							chaine[10]=String.valueOf(Integer.parseInt(chaine[10]+1));
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}else {
							t.add(chaine[0]+";"+chaine[1]+";"+chaine[2]+";"+chaine[3]+";"+chaine[4]+";"+chaine[5]+";"+chaine[6]+";"+chaine[7]+";"+chaine[8]+";"+chaine[9]+";"+chaine[10]+";"+chaine[11]+";"+chaine[12]+";"+chaine[13]+"\n");
						}
					}
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		br.close();
		FileWriter fw = null;
		if(comparator instanceof CompareDate) {
			fw = new FileWriter(chemin + "/data/timeline/timeline.csv",false);
		}else {
			fw = new FileWriter(chemin + "/data/cardline/cardline.csv",false);
		}
		for(String ligne : t){
			fw.write(ligne);
		}
		fw.close();
	}

	public double get_taux(Carte carte) {
		double tauxj = 0;
		double tauxr = 0;
		for (Entry<String, Integer> entry : taux_joue.entrySet()) {
			if (entry.getKey().equals(carte.getNom_Carte())) {
				tauxj = entry.getValue();
			}
		}
		for (Entry<String, Integer> entry : taux_reussi.entrySet()) {
			if (entry.getKey().equals(carte.getNom_Carte())) {
				tauxr = entry.getValue();
			}
		}
		return Math.round(((tauxr / tauxj) * 100) * 10000.0) / 10000.0;
	}

	public void majFichier() {

	}
}
