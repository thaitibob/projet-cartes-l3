package LancementPartie;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.awt.event.ActionEvent;

public class TopJoueurs {

	private JFrame frame;
	private JLabel labelJ1 = new JLabel("");
	private JLabel labelj2 = new JLabel("");
	private JLabel labelj3 = new JLabel("");

	public TopJoueurs() throws IOException {
		initialize();
	}

	private void initialize() throws IOException {
		frame = new JFrame();
		frame.setSize(500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel labelTop = new JLabel("Top 3 Joueurs");
		labelTop.setFont(new Font("Lucida Grande", Font.BOLD, 30));
		labelTop.setForeground(Color.RED);
		labelTop.setBounds(129, 26, 250, 67);
		frame.getContentPane().add(labelTop);
		frame.getContentPane().setBackground(Color.orange);

		labelJ1.setBounds(137, 136, 187, 39);
		frame.getContentPane().add(labelJ1);

		labelj2.setBounds(137, 182, 187, 39);
		frame.getContentPane().add(labelj2);

		labelj3.setBounds(137, 233, 187, 39);
		frame.getContentPane().add(labelj3);

		JButton btnRetourMenu = new JButton("Retour Menu");
		btnRetourMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPrincipal p = new MenuPrincipal();
				frame.dispose();
				frame = null;
				System.gc();
			}
		});
		btnRetourMenu.setBounds(160, 290, 140, 39);
		frame.getContentPane().add(btnRetourMenu);
		frame.setVisible(true);

		calcul_classement();
		frame.setLocationRelativeTo(null);

	}

	private void calcul_classement() throws IOException {
		HashMap<String, Integer> t = new HashMap<String, Integer>();
		String chemin = System.getProperty("user.dir");
		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(chemin + "/data/TopJoueurs.csv"));
			while ((line = br.readLine()) != null) {
				String[] chaine = line.split(";");
				try {
					t.put(chaine[0], Integer.parseInt(chaine[1]));
				} catch (Exception e) {
					// e.printStackTrace();
				}

			}
		} catch (FileNotFoundException e) {
			// e.printStackTrace();
		}
		br.close();

		List<Map.Entry<String, Integer>> comparingByValue = t.entrySet().stream().sorted(Map.Entry.comparingByValue())
				.collect(Collectors.toList());
		labelJ1.setText("Joueur 1 : " + comparingByValue.get(comparingByValue.size() - 1));
		labelj2.setText("Joueur 2 : " + comparingByValue.get(comparingByValue.size() - 2));
		labelj3.setText("Joueur 3 : " + comparingByValue.get(comparingByValue.size() - 3));

	}

}
