package LancementPartie;
import java.awt.Color;
import JeuGraphique.*;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Comparator.CompareDate;
import GestionFichiers.SauvegardePartie;
import InterfaceCarte.Carte;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

public class InscriptionJ {
	static Difficulte d = null;

	public InscriptionJ(Comparator<Carte> comparator) {
		ArrayList<Joueur> joueur = new ArrayList<Joueur>();

		Object[] row = new Object[2];

		// create JFrame and JTable
		JFrame frame = new JFrame();
		JTable table = new JTable();
		
		frame.getContentPane().setBackground(Color.orange);

		// create a table model and set a Column Identifiers to this model
		Object[] columns = { "Pseudo", "Age" };
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(columns);

		// set the model to the table
		table.setModel(model);

		// Change A JTable Background Color, Font Size, Font Color, Row Height
		table.setBackground(Color.LIGHT_GRAY);
		table.setForeground(Color.black);
		Font font = new Font("", 1, 22);
		table.setFont(font);
		table.setRowHeight(30);

		// create JTextFields
		JTextField pseudo = new JTextField();
		JTextField textAge = new JTextField();

		// create JButtons
		JButton btnAdd = new JButton("Ajout");
		JButton btnDelete = new JButton("Supprimer");

		pseudo.setBounds(78, 220, 100, 25);
		textAge.setBounds(78, 272, 100, 25);

		btnAdd.setBounds(190, 220, 100, 25);
		btnDelete.setBounds(190, 274, 100, 25);

		// create JScrollPane
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(32, 8, 446, 200);

		frame.getContentPane().setLayout(null);

		frame.getContentPane().add(pane);

		// add JTextFields to the jframe
		frame.getContentPane().add(pseudo);
		frame.getContentPane().add(textAge);

		// add JButtons to the jframe
		frame.getContentPane().add(btnAdd);
		frame.getContentPane().add(btnDelete);

		JLabel pseudolabel = new JLabel("Pseudo");
		pseudolabel.setBounds(21, 225, 61, 16);
		frame.getContentPane().add(pseudolabel);

		JLabel Agelabel = new JLabel("Age");
		Agelabel.setBounds(21, 274, 61, 16);
		frame.getContentPane().add(Agelabel);

		JButton Bot = new JButton("Ajout Bot");
		Bot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (joueur.size() < 8) {
					int compteur = 0;
					for (Joueur j : joueur) {
						if (j instanceof Bot) {
							compteur++;
						}
					}
					row[0] = "Bot" + compteur;
					row[1] = "";
					try {
						joueur.add(new Bot("Bot" + String.valueOf(compteur),comparator));
					} catch (NumberFormatException | IOException e1) {
						e1.printStackTrace();
					}
					model.addRow(row);
				} else {
					JOptionPane.showMessageDialog(frame, "8 joueurs max");
				}

			}
		});
		Bot.setBounds(349, 222, 100, 25);
		frame.getContentPane().add(Bot);

		ActionListener ChoixDifficulte = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getActionCommand().equals("facile")) {
					d=Difficulte.facile;
				}
				if (e.getActionCommand().equals("moyen")) {
					d=Difficulte.facile;
				}
				if (e.getActionCommand().equals("difficile")) {
					d=Difficulte.facile;
				}
				if (e.getActionCommand().equals("Personnalisé")) {
					d=Difficulte.facile;
				}
			}
		};

		JRadioButton bouttonfacile = new JRadioButton("facile");
		bouttonfacile.setBounds(325, 247, 141, 23);
		bouttonfacile.addActionListener(ChoixDifficulte);
		frame.getContentPane().add(bouttonfacile);

		JRadioButton bouttondifficile = new JRadioButton("difficile");
		bouttondifficile.setBounds(325, 302, 141, 23);
		bouttondifficile.addActionListener(ChoixDifficulte);
		frame.getContentPane().add(bouttondifficile);

		JRadioButton bouttonmoyen = new JRadioButton("moyen");
		bouttonmoyen.setBounds(325, 273, 141, 23);
		bouttonmoyen.addActionListener(ChoixDifficulte);
		frame.getContentPane().add(bouttonmoyen);

		JRadioButton bouttonpersonnalise = new JRadioButton("Personnalisé");
		bouttonpersonnalise.setBounds(325, 327, 141, 23);
		bouttonpersonnalise.addActionListener(ChoixDifficulte);
		frame.getContentPane().add(bouttonpersonnalise);

		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					int age = Integer.parseInt(textAge.getText());
					if (joueur.size() < 8) {
						int compteur=0;
						row[0] = pseudo.getText();
						row[1] = textAge.getText();
						for(Joueur j : joueur) {
							if(j.getNom_joueur().equals(row[0])) {
								compteur++;
							}
						}
						if(compteur!=0) {
							JOptionPane.showMessageDialog(frame, "Joueur déjà présent");
						}else {
							model.addRow(row);
							joueur.add(new Joueur(pseudo.getText(), age));
							pseudo.setText("");
							textAge.setText("");
						}
					} else {
						JOptionPane.showMessageDialog(frame, "8 joueurs max");
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(frame, "erreur age");
				}
			}
		});

		// button remove row
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// i = the index of the selected row
				int i = table.getSelectedRow();
				Joueur tmp = null;
				if (i >= 0) {
					for (Joueur j : joueur) {
						if (model.getValueAt(i, 0).toString().equals(j.getNom_joueur())) {
							tmp = j;
						}
					}
					joueur.remove(tmp);
					model.removeRow(i);
				}
			}
		});

		// get selected row data From table to textfields
		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {

				int i = table.getSelectedRow();

				pseudo.setText(model.getValueAt(i, 0).toString());
				textAge.setText(model.getValueAt(i, 1).toString());
			}
		});

		frame.setSize(500, 400);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

		ButtonGroup group = new ButtonGroup();
		group.add(bouttonfacile);
		group.add(bouttonmoyen);
		group.add(bouttondifficile);
		group.add(bouttonpersonnalise);

		JButton buttonValidation = new JButton("Valider");
		buttonValidation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int compteur=0;
				for(Joueur j : joueur) {
					if(j instanceof Bot) {
						compteur++;
					}
				}
				if(compteur==0 && joueur.size() > 1) {
					try {
						VueJeu v = new VueJeu(joueur, comparator);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				else if (d != null && joueur.size() > 1) {
					for(Joueur j : joueur) {
						if(j instanceof Bot) {
							((Bot) j).setD(d);
						}
					}
					try {
						VueJeu v = new VueJeu(joueur, comparator);
						frame.dispose();
						System.gc();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else {
					JOptionPane.showMessageDialog(frame, "Selectionner la difficulté ou 2 joueurs mini");
				}
			}
		});
		buttonValidation.setBounds(88, 312, 169, 39);
		frame.getContentPane().add(buttonValidation);

	}
}