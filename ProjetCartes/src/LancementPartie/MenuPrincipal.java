package LancementPartie;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import Comparator.CompareDate;
import GestionFichiers.SauvegardePartie;
import JeuGraphique.JeuG;
import JeuGraphique.VueJeu;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class MenuPrincipal {

	private JFrame frame;


	/**
	 * Create the application.
	 */
	public MenuPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setVisible(true);
		frame.getContentPane().setForeground(Color.ORANGE);
		frame.setSize(500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setBackground(Color.ORANGE);
		
		JLabel Titre = new JLabel("TIMELINE & CARDLINE");
		Titre.setForeground(Color.RED);
		Titre.setFont(new Font("Lucida Grande", Font.BOLD, 30));
		Titre.setBounds(72, 20, 366, 54);
		frame.getContentPane().add(Titre);
		
		JLabel Nom1 = new JLabel("Jonathan Valle");
		Nom1.setFont(new Font("Serif", Font.ITALIC, 18));
		Nom1.setBounds(328, 303, 138, 34);
		frame.getContentPane().add(Nom1);
		
		JLabel nom2 = new JLabel("Léonard Basaran");
		nom2.setFont(new Font("Serif", Font.ITALIC, 18));
		nom2.setBounds(328, 338, 138, 34);
		frame.getContentPane().add(nom2);
		
		JButton bouttonNouvellePartieTimeline = new JButton("Nouvelle Partie Timeline");
		bouttonNouvellePartieTimeline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscriptionJ i = new InscriptionJ(new CompareDate());
				frame.dispose();
				frame = null;
				System.gc();
			}
		});
		bouttonNouvellePartieTimeline.setBounds(153, 115, 184, 44);
		frame.getContentPane().add(bouttonNouvellePartieTimeline);
		
		JButton BouttonTopJoueurs = new JButton("Top Joueurs");
		BouttonTopJoueurs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					TopJoueurs t = new TopJoueurs();
					frame.dispose();
					frame = null;
					System.gc();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		BouttonTopJoueurs.setBounds(174, 246, 141, 29);
		frame.getContentPane().add(BouttonTopJoueurs);
		
		JLabel labelPromo = new JLabel("L3 MIASHS");
		labelPromo.setFont(new Font("Serif", Font.ITALIC, 27));
		labelPromo.setBounds(19, 313, 178, 44);
		frame.getContentPane().add(labelPromo);
		
		JButton buttonChargement = new JButton("Charger Partie");
		buttonChargement.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SauvegardePartie s = new SauvegardePartie();
				JeuG v=null;
				try {
					v = s.chargement();
				} catch (ClassNotFoundException | IOException e2) {
					e2.printStackTrace();
				}
				if (v == null) {
					JOptionPane.showMessageDialog(frame, "Erreur de chargement (verifier bien le fichier)");
				} else {
					try {
						VueJeu t = new VueJeu(v);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					frame.dispose();
					frame = null;
					System.gc();
				}
			}
		});
		buttonChargement.setBounds(174, 217, 141, 29);
		frame.getContentPane().add(buttonChargement);
		
		JButton buttonNouvellePartieCardline = new JButton("Nouvelle Partie CardLine");
		buttonNouvellePartieCardline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChoixCardline i = new ChoixCardline();
				frame.dispose();
				frame = null;
				System.gc();
			}
		});
		buttonNouvellePartieCardline.setBounds(153, 161, 184, 44);
		frame.getContentPane().add(buttonNouvellePartieCardline);
		frame.setLocationRelativeTo(null);
	}
}
