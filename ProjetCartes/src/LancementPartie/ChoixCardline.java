package LancementPartie;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import Comparator.ComparatorPIB;
import Comparator.ComparatorPollution;
import Comparator.ComparatorPopulation;
import Comparator.ComparatorSuperficie;
import Comparator.CompareDate;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChoixCardline {

	private JFrame frame;

	/**
	 * Create the application.
	 */
	public ChoixCardline() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(500, 400);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(Color.ORANGE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		JButton btnPopulation = new JButton("Classement Population");
		btnPopulation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscriptionJ i = new InscriptionJ(new ComparatorPopulation());
				frame.dispose();
				frame = null;
				System.gc();
			}
		});
		btnPopulation.setBounds(148, 89, 172, 46);
		frame.getContentPane().add(btnPopulation);
		
		JButton btnPIB = new JButton("Classement PIB");
		btnPIB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscriptionJ i = new InscriptionJ(new ComparatorPIB());
				frame.dispose();
				frame = null;
				System.gc();
			}
		});
		btnPIB.setBounds(148, 147, 172, 46);
		frame.getContentPane().add(btnPIB);
		
		JButton btnPollution = new JButton("Classement Pollution");
		btnPollution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscriptionJ i = new InscriptionJ(new ComparatorPollution());
				frame.dispose();
				frame = null;
				System.gc();
			}
		});
		btnPollution.setBounds(148, 205, 172, 46);
		frame.getContentPane().add(btnPollution);
		
		JButton btnSuperficie = new JButton("Classement Superficie");
		btnSuperficie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscriptionJ i = new InscriptionJ(new ComparatorSuperficie());
				frame.dispose();
				frame = null;
				System.gc();
			}
		});
		btnSuperficie.setBounds(148, 263, 172, 46);
		frame.getContentPane().add(btnSuperficie);
		
		JLabel lblCardline = new JLabel("CARDLINE");
		lblCardline.setForeground(Color.RED);
		lblCardline.setFont(new Font("Lucida Grande", Font.BOLD, 25));
		lblCardline.setBounds(164, 21, 172, 45);
		frame.getContentPane().add(lblCardline);
	}
}
