package Comparator;

import java.io.Serializable;
import java.util.Comparator;

import InterfaceCarte.Carte;
import InterfaceCarte.Invention;
import InterfaceCarte.Pays;

public class ComparatorSuperficie implements Comparator<Carte>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int compare(Carte o1, Carte o2) {
		Pays tmp1 = (Pays) o1;
		Pays tmp2 = (Pays) o2;
		if (tmp1.getSuperficie() < tmp2.getSuperficie()) {
			return -1;
		}
		if (tmp1.getSuperficie() > tmp2.getSuperficie()) {
			return 1;
		}
		return 0;
	}
	
}
