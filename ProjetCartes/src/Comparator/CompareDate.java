package Comparator;


import JeuGraphique.*;

import java.io.Serializable;
import java.util.Comparator;

import InterfaceCarte.Carte;
import InterfaceCarte.Invention;

public class CompareDate implements Comparator<Carte>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int compare(Carte o1, Carte o2) {
		Invention tmp1 = (Invention) o1;
		Invention tmp2 = (Invention) o2;
		if (tmp1.getDate_invention() < tmp2.getDate_invention()) {
			return -1;
		}
		if (tmp1.getDate_invention() > tmp2.getDate_invention()) {
			return 1;
		}
		return 0;
	}
	
}
