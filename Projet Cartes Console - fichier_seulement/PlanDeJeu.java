package jeu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PlanDeJeu {
	ArrayList<Invention> l = new ArrayList<Invention>();
	Comparator<Invention> comparator;
	
	public PlanDeJeu(Invention l, Comparator<Invention> comparator) {
		this.l.add(l);
		this.comparator = comparator;
	}
	
	// essaye de placer une carte
	public boolean ajout_carte(Invention carte, int position) {
		int pos = Collections.binarySearch(l, carte, comparator);
		if(pos<0) {
			pos=-pos-1;
			if(pos==position) {
				l.add(pos, carte);
				return true;
			}
		}else {
			// cas ou il y a même valeur sur plateau et à poser donc gauche ou droite
			if(pos==position-1 || pos == position) {
				l.add(pos, carte);
				return true;
			}
			
		}
		return false;
	}

	@Override
	public String toString() {
		return "PlanDeJeu [l=" + l + "]";
	}
	
	
}
