package jeu;

import java.util.ArrayList;

public class JeuDeCartes {
	ArrayList<Invention> pioche = new ArrayList<Invention>();
	ArrayList<Invention> defausse = new ArrayList<Invention>();
	
	public JeuDeCartes(ArrayList<Invention> pioche) {
		this.pioche = pioche;
	}
	
	public void ajout_defausse(Invention carte) {
		defausse.add(carte);
	}
	
	// A verif s'il n'y a plus de cartes sinon retourne la carte en tête de pioche
	public Invention pioche() {
		Invention distribution=pioche.get(0);
		pioche.remove(0);
		return distribution;
	}
	
	public int taille_pioche() {
		return pioche.size();
	}

	@Override
	public String toString() {
		return "JeuDeCartes [pioche=" + pioche + ", defausse=" + defausse + "]";
	}
	
	
}
