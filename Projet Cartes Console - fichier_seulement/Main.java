package jeu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		
		class CompareDate implements Comparator<Invention> {

			@Override
			public int compare(Invention o1, Invention o2) {
				if (o1.getDate_invention() < o2.getDate_invention()) {
					return -1;
				}
				if (o1.getDate_invention() > o2.getDate_invention()) {
					return 1;
				}
				return 0;
			}
		}
		
		ArrayList<Joueur> joueurs = new ArrayList<Joueur>();

		LectureFichiers l = new LectureFichiers();
		JeuDeCartes jeu_carte = new JeuDeCartes(l.getInventions());

		Scanner sc = new Scanner(System.in);
		System.out.println("nombre de joueurs : ");
		int nb_joueurs = sc.nextInt();
		for (int i = 0; i < nb_joueurs; i++) {
			Scanner nom1 = new Scanner(System.in);
			System.out.println("nom joueur : ");
			String nom = nom1.nextLine();
			System.out.println("age joueur : ");
			int age = sc.nextInt();
			joueurs.add(new Joueur(nom, age));
		}
		
		// attention distribution des cartes en fonction de l'experience de jeu

		if (nb_joueurs == 2 || nb_joueurs == 3) {
			for(Joueur j : joueurs) {
				for(int i = 0 ; i<6 ; i++) {
					j.ajout_deck(jeu_carte.pioche());
				}
			}
		}

		if (nb_joueurs == 4 || nb_joueurs == 5) {
			for(Joueur j : joueurs) {
				for(int i = 0 ; i<5 ; i++) {
					j.ajout_deck(jeu_carte.pioche());
				}
			}
		}

		if (nb_joueurs == 6 || nb_joueurs == 7 || nb_joueurs == 8) {
			for(Joueur j : joueurs) {
				for(int i = 0 ; i<4 ; i++) {
					j.ajout_deck(jeu_carte.pioche());
				}
			}
		}
		
		System.out.println(jeu_carte.taille_pioche());
		for(Joueur j : joueurs) {
			System.out.println(j.toString());
			System.out.println(j.taille_deck());
		}
		
		Jeu j = new Jeu(jeu_carte,joueurs, new CompareDate());
		while(!j.gagnant_final()) {
			j.tour_de_jeu();
		}

	}

}
