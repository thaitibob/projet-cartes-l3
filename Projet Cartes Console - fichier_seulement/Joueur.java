package jeu;

import java.util.ArrayList;

public class Joueur {
	
	private String nom_joueur;
	private int age;
	private ArrayList<Invention> cartes_joueur = new ArrayList<Invention>();
	private boolean peux_jouer;
	
	
	public Joueur(String nom_joueur, int age) {
		this.nom_joueur = nom_joueur;
		this.age = age;
		this.peux_jouer=true;
	}
	
	public boolean isPeux_jouer() {
		return peux_jouer;
	}

	public void setPeux_jouer(boolean peux_jouer) {
		this.peux_jouer = peux_jouer;
	}

	public void ajout_deck(Invention carte) {
		cartes_joueur.add(carte);
	}
	
	public int taille_deck() {
		return cartes_joueur.size();
	}
	
	//recupère la carte que l'on souhaite poser
	public Invention recupere_carte_position(int n) {
		return cartes_joueur.get(n);
	}
	
	public Invention supprime_carte_position(int n) {
		return cartes_joueur.get(n);
	}

	@Override
	public String toString() {
		return "Joueur [nom_joueur=" + nom_joueur + ", age=" + age + ", cartes_joueur=" + cartes_joueur + "]";
	}
	
	
}
