package jeu;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Jeu {
	JeuDeCartes jeu_cartes;
	ArrayList<Joueur> liste_joueurs;
	PlanDeJeu p;

	public Jeu(JeuDeCartes j, ArrayList<Joueur> liste_joueurs, Comparator<Invention> c) {
		this.jeu_cartes = j;
		this.liste_joueurs = liste_joueurs;
		p = new PlanDeJeu(this.jeu_cartes.pioche(), c);
	}

	private void elimination() {
		for (Joueur j : liste_joueurs) {
			if (j.taille_deck() != 0 && j.isPeux_jouer()==true) {
				j.setPeux_jouer(false);
			}
			if (j.taille_deck()==0 && j.isPeux_jouer()) {
				j.ajout_deck(jeu_cartes.pioche());
			}
		}
	}
	
	//condition s'il y a un seul vainqueur
	public boolean gagnant_final() {
		int compteur=0;
		for(Joueur j : liste_joueurs) {
			if(j.isPeux_jouer())
				compteur++;
		}
		return compteur ==1 ? true : false;
	}

	public boolean tour_de_jeu() {	
		boolean gagnant = false;
		for (Joueur j : liste_joueurs) {
			if(j.isPeux_jouer()) {
				System.out.println("affichage du plan de jeu : ");
				System.out.println(p.toString());
				System.out.println("affichage des cartes du joueur : ");
				System.out.println(j.toString());
				Scanner numero = new Scanner(System.in);
				System.out.println("numéro de cartes à jouer : ");
				// faire les vérif du nombre de cartes max blabla
				// le n compte de 0 à n-1
				int n = numero.nextInt();
				System.out.println(j.recupere_carte_position(n));
				System.out.println("placement de la carte : ");
				// n2 compte de 0 à n
				int n2 = numero.nextInt();
				// le numéro doit être compris entre 0 et la taille du plan de jeu (n+1
				// insertion possible)
				// a verif cette partie ?????
				if (!p.ajout_carte(j.recupere_carte_position(n), n2)) {
					j.supprime_carte_position(n);
					j.ajout_deck(jeu_cartes.pioche());
					System.out.println("Mauvais placement");
					System.out.println("Cartes actuelles : "+j.toString());
					System.out.println("Defausse : "+p.toString());
				} else {
					System.out.println("bien joué");
					j.supprime_carte_position(n);
				}
				if(j.taille_deck()==0) {
					gagnant=true;
				}
			}	
		}
		// s'il y a un gagnant dans le tour alors il y a des éliminés
		if(gagnant) {
			elimination();
		}
		

		return false;
	}

}
