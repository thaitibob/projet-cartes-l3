package jeu;

public class Invention {
	String nom_invention;
	int date_invention;
	String image_lien;
	String image_lien_date;
	
	public Invention(String nom_invention, int date_invention, String image_lien, String image_lien_date) {
		super();
		this.nom_invention = nom_invention;
		this.date_invention = date_invention;
		this.image_lien = image_lien;
		this.image_lien_date = image_lien_date;
	}
	
	public int getDate_invention() {
		return date_invention;
	}

	@Override
	public String toString() {
		return "Invention [nom_invention=" + nom_invention + ", date_invention=" + date_invention + ", image_lien="
				+ image_lien + ", image_lien_date=" + image_lien_date + "]";
	}
	
	
	
}
