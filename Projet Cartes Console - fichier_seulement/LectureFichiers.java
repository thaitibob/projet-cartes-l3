package jeu;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class LectureFichiers {
	ArrayList<Invention> inventions = new ArrayList<Invention>();

	public LectureFichiers() throws IOException {
		// fichier enregistré : /Users/jonathan/Documents/Projet java
		// 2018/data/timeline/timeline.csv
		try {
			BufferedReader fichier_source = new BufferedReader(
					new FileReader("/Users/jonathan/Documents/Projet java 2018/data/timeline/timeline.csv"));
			String chaine;
			int i = 1;

			while ((chaine = fichier_source.readLine()) != null) {
				if (i > 1) {
					// ajout d'un Utilisateur
					String[] tabChaine = chaine.split(";");
					// System.out.println(tabChaine[0]+" "+Integer.parseInt(tabChaine[1])+"
					// "+tabChaine[2]);
					inventions.add(new Invention(tabChaine[0], Integer.parseInt(tabChaine[1]), tabChaine[2] + ".jpeg",
							tabChaine[2] + "_date.jpeg"));
				}
				i++;
			}
			fichier_source.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier est introuvable !");
			System.out.println(e);
		}
		// permet de mélanger l'arrayList
		Collections.shuffle(inventions);
	}

	public ArrayList<Invention> getInventions() {
		return inventions;
	}
	
	

}
